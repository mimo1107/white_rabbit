<?php

class WhiteRabbit2
{
    private $coins = [100,50,20,10,5,2,1];

    /**
     * return a php array, that contains the amount of each type of coin, required to fulfill the amount.
     * The returned array should use as few coins as possible.
     * The coins available for use is: 1, 2, 5, 10, 20, 50, 100
     * You can assume that $amount will be an int
     * @param $amount
     * @return array
     */
    public function findCashPayment($amount){
        $result = array();
        //For each loop running through each coins, starting with the greatest first
        foreach ($this->coins as $coin){
            $count = 0;
            if($amount >= $coin){
                $count = floor($amount/$coin);
                $amount -= $count*$coin;
            }
            //Adding to the $result array with $coin as key and times able to divide by as value
            $result[$coin] = $count;
        }
        return $result;
    }
}