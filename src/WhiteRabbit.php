<?php

class WhiteRabbit {

    public function findMedianLetterInFile($filePath) {
        return $this->findMedianLetter($this->parseFile($filePath));
    }

    /**
     * Parse the input file for letters.
     * @param $filePath
     * @return string
     */
    private function parseFile ($filePath) {
        if($filePath){
            $content = file_get_contents($filePath);
            if($content){
                //Use regex to remove all non alphabetic chars
                $content = preg_replace("/[^[:alpha:]]/u", '', strtolower($content));
                //Count amount of each char and return an array with char as key and count as value
                $lettersArray = count_chars($content,1);
                //Sort the array
                asort($lettersArray);
                return $lettersArray;
            }
            echo "Could not get file content" . PHP_EOL ;
        } else {
            echo "No path specified" . PHP_EOL ;
        }
        exit;
    }

    /**
     * Return the letter whose occurrences are the median.
     * @param $parsedFile
     * @return array
     */
    private function findMedianLetter($parsedFile) {
        //Length of the array
        $length = count($parsedFile);
        $occurrences = array_values($parsedFile);
        $letters = array_keys($parsedFile);
        //If length is odd select only one median by add one to length
        if($length % 2 != 0){
            $length++;
        }
        return array("letter"=>chr($letters[$length/2]),"count"=>$occurrences[$length/2]);
    }
}